# Notes

## iOS builds

After doing the iOS build, you need to go and manually specify that camera
is allowed.

Open `Info.plist` in the build folder, and add this setting:

```
<key>NSCameraUsageDescription</key>
<string>QRCode scanning for card detection</string>
```

See also:

- https://forum.unity.com/threads/where-can-i-add-nscamerausagedescription-persmission.445973/
- https://forum.unity.com/threads/ios-requests-camera-description.471094/
