namespace Names
{
    [System.Serializable]
    public class WorldAnimalName
    {
        public TerraAnimalis.WorldAnimal worldAnimal;
        public string rootName;
    }
}