using System.Collections.Generic;

namespace Names
{
    [System.Serializable]
    public class NameConfiguration
    {
        public List<WorldAnimalName> worldAnimalNames;
    }
}