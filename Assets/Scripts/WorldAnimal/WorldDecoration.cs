﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TerraAnimalis
{
    /// <summary>
    /// Something that can be attached to an attachment of a world animal
    /// </summary>
    public class WorldDecoration : MonoBehaviour
    {
        [SerializeField, Tooltip("The type of attachment this decoration is looking for")]
        private AttachmentType attachmentType;

        [SerializeField, Tooltip("The adjective to use if this decoration is attached to the world animal.")]
        private string adjective;

        private List<WorldAttribute> attributeList;
        
        public AttachmentType AttachmentType { get { return attachmentType; } }

        public List<WorldAttribute> AttributeList { get { return attributeList; } }

        public WorldAttachment Attachment { get; set; }

        public string DecorationName { get; private set; }
        /// <summary>
        /// The unique ID of this decoration (Two trees will each have their own unique ID)
        /// </summary>
        public string QRCode { get; private set; }
        /// <summary>
        /// The adjective to use if this decoration is attached to the world animal.
        /// </summary>
        public string Adjective { get { return adjective; } }
        

        public void Configure(ObjectStatInfo statInfo)
        {
            QRCode = statInfo.QRCode;
            DecorationName = statInfo.ObjectName;

            attributeList = new List<WorldAttribute>();
            if(statInfo.Stat1Value != 0)
                attributeList.Add(new WorldAttribute { Attribute = statInfo.Stat1Type, Amount = statInfo.Stat1Value });
            if(statInfo.Stat2Value != 0)
                attributeList.Add(new WorldAttribute { Attribute = statInfo.Stat2Type, Amount = statInfo.Stat2Value });
        }
    }
}