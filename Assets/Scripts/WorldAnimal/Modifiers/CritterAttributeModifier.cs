﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

namespace TerraAnimalis
{
    /// <summary>
    /// Finds every critter with a certain score and adds to the total score with their score * by the specified multiplier.
    /// </summary>
    public class CritterAttributeModifier : ModiferBase
    {
        public enum AmountType
        {
            BothPositiveAndNegativeAmounts,
            OnlyPositiveAmounts,
            OnlyNegativeAmounts,
        }

        [SerializeField]
        private string TargetAttribute;
        [SerializeField]
        private AmountType amountType = AmountType.BothPositiveAndNegativeAmounts;
        [SerializeField]
        private int Multiplier = 1;

        public override int ModifyScore(WorldAnimal worldAnimal, int totalScore, bool debugGUI, ref StringBuilder statsDebugString, ref StringBuilder scoreDebugString)
        {
            if (debugGUI)
            {
                statsDebugString.AppendFormat("For each critter with the {0} attribute", TargetAttribute);
                if (amountType == AmountType.OnlyPositiveAmounts)
                    statsDebugString.Append(" and that has a positive number");
                else if (amountType == AmountType.OnlyNegativeAmounts)
                    statsDebugString.Append(" and that has a negative number");

                statsDebugString.AppendFormat(" Multiply that score by {0}", Multiplier);

                scoreDebugString.AppendFormat("For critters with {0} attributes,", TargetAttribute);
                if (amountType == AmountType.OnlyPositiveAmounts)
                    scoreDebugString.Append(" with a positive number,");
                else if (amountType == AmountType.OnlyNegativeAmounts)
                    scoreDebugString.Append(" with a negative number,");
                scoreDebugString.AppendFormat(" multiply that number by {0}", Multiplier);

                scoreDebugString.AppendLine();
            }

            int scoreAdded = 0;

            var critters = worldAnimal.Critters;
            for (int i = 0; i < critters.Count; i++)
            {
                var currentCritter = critters[i];

                foreach (var attribute in currentCritter.AttributeList)
                {
                    if (attribute.Attribute == TargetAttribute && attribute.Amount != 0)
                    {
                        bool isPositive = attribute.Amount > 0;
                        bool validScore = (amountType == AmountType.BothPositiveAndNegativeAmounts) || (amountType == AmountType.OnlyPositiveAmounts && isPositive) || (amountType == AmountType.OnlyNegativeAmounts && !isPositive);

                        if (validScore)
                        {
                            int additionalScore = attribute.Amount * Multiplier;
                            scoreAdded += additionalScore;
                            totalScore += additionalScore;

                            if (debugGUI)
                            {
                                scoreDebugString.AppendFormat("{0} | {1} x {2} = {3}", currentCritter.CritterName, attribute.Amount, Multiplier, additionalScore);
                                scoreDebugString.AppendLine();
                            }
                        }
                    }
                }
            }

            if (debugGUI)
            {
                scoreDebugString.AppendFormat("Total score added is {0}", scoreAdded);
                scoreDebugString.AppendLine();
            }

            return totalScore;
        }
    }
}