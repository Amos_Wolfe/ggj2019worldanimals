﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

namespace TerraAnimalis
{
    /// <summary>
    /// Modifies the total score based on the rules of the modifier
    /// </summary>
    public abstract class ModiferBase : MonoBehaviour
    {
        public abstract int ModifyScore(WorldAnimal worldAnimal, int totalScore, bool debugGUI, ref StringBuilder statsDebugString, ref StringBuilder scoreDebugString);
    }
}