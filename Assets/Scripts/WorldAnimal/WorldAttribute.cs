﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class WorldAttribute
{
    public string Attribute;
    public int Amount;
}
