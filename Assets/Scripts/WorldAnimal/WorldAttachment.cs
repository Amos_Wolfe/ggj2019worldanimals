﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TerraAnimalis
{
    /// <summary>
    /// Something that a world decoration can attach to.
    /// </summary>
    public class WorldAttachment : MonoBehaviour
    {
        [SerializeField]
        private AttachmentType attachmentType;

        private WorldDecoration currentDecoration;

        /// <summary>
        /// Where this attachment is located on the world animal.
        /// </summary>
        public AttachmentType AttachmentType { get { return attachmentType; } }

        public WorldDecoration CurrentDecoration { get { return currentDecoration; } }

        public void AttachDecoration(WorldDecoration newDecoration)
        {
            if (currentDecoration != null)
            {
                Debug.LogError("[WorldAttachment] This attachment is full");
                return;
            }
            var decorationTransform = newDecoration.transform;
            decorationTransform.SetParent(transform, true);
            decorationTransform.localPosition = Vector3.zero;
            decorationTransform.localRotation = Quaternion.identity;

            currentDecoration = newDecoration;
            currentDecoration.Attachment = this;
        }

        public void RemoveDecoration()
        {
            if (currentDecoration == null)
            {
                Debug.LogError("[WorldAttachment] No decoration to remove");
                return;
            }

            Destroy(currentDecoration.gameObject);
            currentDecoration = null;
        }

        private void OnDrawGizmos()
        {
            switch (attachmentType)
            {
                case AttachmentType.InFront:
                    Gizmos.color = Color.green;
                    break;
                case AttachmentType.OnTop:
                    Gizmos.color = new Color(0, 0.5f, 1.0f);
                    break;
                case AttachmentType.Satellite:
                    Gizmos.color = new Color(1.0f, 0.5f, 0.0f);
                    break;
            }


            Transform thisTransform = transform;
            Vector3 thisPos = thisTransform.position;
            Gizmos.DrawWireSphere(thisPos, 0.05f);
            Gizmos.DrawLine(thisPos, thisPos + thisTransform.up * 0.2f);
        }
    }
}