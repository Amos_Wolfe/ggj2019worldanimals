﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TerraAnimalis
{
    public class WorldAnimal : MonoBehaviour
    {
        [SerializeField]
        private SpriteRenderer worldAnimalImage;

        [SerializeField, Tooltip("The name that will appear at the top of the screen while the animal is being used.")]
        private string animalName;
        
        private List<WorldAttribute> baseAttributeList;

        private WorldDecorationFactory worldDecorationFactory;
        private CritterFactory critterFactory;
        private ScoringManager scoringManager;

        private List<WorldAttachment> decorationAttachmentList;
        private List<CritterAttachment> critterAttachmentList;

        private List<WorldDecoration> decorationList;
        private List<Critter> critterList;

        private List<ModiferBase> modifierList;

        public List<WorldAttribute> BaseAttributes { get { return baseAttributeList; } }
        public List<WorldDecoration> Decorations { get { return decorationList; } }
        public List<Critter> Critters { get { return critterList; } }
        public List<ModiferBase> Modifiers { get { return modifierList; } }

        /// <summary>
        /// The name that will appear at the top of the screen while the animal is being used.
        /// </summary>
        public string AnimalName { get { return animalName; } }

        private void Awake()
        {
            worldDecorationFactory = GameObject.FindObjectOfType<WorldDecorationFactory>();
            critterFactory = GameObject.FindObjectOfType<CritterFactory>();
            scoringManager = GameObject.FindObjectOfType<ScoringManager>();
            modifierList = new List<ModiferBase>(GetComponentsInChildren<ModiferBase>());

            decorationAttachmentList = new List<WorldAttachment>(GetComponentsInChildren<WorldAttachment>());

            critterAttachmentList = new List<CritterAttachment>(GetComponentsInChildren<CritterAttachment>());

            decorationList = new List<WorldDecoration>();
            critterList = new List<Critter>();
        }

        public void Configure(ObjectStatInfo statInfo)
        {
            baseAttributeList = new List<WorldAttribute>();
            if(statInfo.Stat1Value != 0)
                baseAttributeList.Add(new WorldAttribute { Attribute = statInfo.Stat1Type, Amount = statInfo.Stat1Value });
            if(statInfo.Stat2Value != 0)
                baseAttributeList.Add(new WorldAttribute { Attribute = statInfo.Stat2Type, Amount = statInfo.Stat2Value });

            scoringManager.UpdateName(this);
            scoringManager.UpdateScore(this);
        }

        public void AttachOrRemoveDecoration(ObjectStatInfo decorationInfo)
        {
            var existingDecoration = FindExistingDecoration(decorationInfo.QRCode);
            if (existingDecoration != null)
            {
                // Remove an existing decoration
                var attachment = existingDecoration.Attachment;
                if (attachment == null)
                {
                    Debug.LogError("[World Animal] Could not find attachment for decoration {0}", existingDecoration);
                    return;
                }

                decorationList.Remove(existingDecoration);
                attachment.RemoveDecoration();
            }
            else
            {
                // Add a new decoration
                var newDecoration = worldDecorationFactory.CreateDecoration(decorationInfo.ObjectName);
                newDecoration.Configure(decorationInfo);

                var attachment = FindAvailableDecorationAttachment(newDecoration.AttachmentType);
                if (attachment == null)
                {
                    Debug.LogErrorFormat("[WorldAnimal] No more available attachments of type {0} available", newDecoration.AttachmentType);
                    Destroy(newDecoration.gameObject);
                    return;
                }

                decorationList.Add(newDecoration);
                attachment.AttachDecoration(newDecoration);
            }

            scoringManager.UpdateName(this);
            scoringManager.UpdateScore(this);
        }

        public void AttachOrRemoveCritter(ObjectStatInfo critterInfo)
        {
            var existingCritter = FindExistingCritter(critterInfo.QRCode);
            if (existingCritter != null)
            {
                // Remove an existing critter
                var attachment = existingCritter.Attachment;
                if (attachment == null)
                {
                    Debug.LogError("[World Animal] Could not find attachment for critter {0}", existingCritter);
                    return;
                }

                critterList.Remove(existingCritter);
                attachment.RemoveCritter();
            }
            else
            {
                // Add a new critter
                var newCritter = critterFactory.CreateCritter(critterInfo.ObjectName);
                newCritter.Configure(critterInfo);

                var attachment = FindAvailableCritterAttachment();
                if (attachment == null)
                {
                    Debug.LogError("[WorldAnimal] No more available critter attachments");
                    Destroy(newCritter.gameObject);
                    return;
                }

                critterList.Add(newCritter);
                attachment.AttachCritter(newCritter);
            }

            scoringManager.UpdateName(this);
            scoringManager.UpdateScore(this);
        }

        private WorldDecoration FindExistingDecoration(string qrCode)
        {
            for (int i = 0; i < decorationList.Count; i++)
            {
                var currentDecoration = decorationList[i];
                if (currentDecoration.QRCode == qrCode)
                    return currentDecoration;
            }

            return null;
        }

        private Critter FindExistingCritter(string qrCode)
        {
            for (int i = 0; i < critterList.Count; i++)
            {
                var currentCritter = critterList[i];
                if (currentCritter.QRCode == qrCode)
                    return currentCritter;
            }

            return null;
        }

        private WorldAttachment FindAvailableDecorationAttachment(AttachmentType attachmentType)
        {
            if (decorationAttachmentList.Count == 0)
            {
                Debug.LogError("[WorldAnimal] No decoration attachments available");
                return null;
            }

            int randomOffset = UnityEngine.Random.Range(0, decorationAttachmentList.Count);
            for (int i = 0; i < decorationAttachmentList.Count; i++)
            {
                int offsetI = (i + randomOffset) % decorationAttachmentList.Count;

                var currentAttachment = decorationAttachmentList[i];
                if (currentAttachment.AttachmentType == attachmentType && currentAttachment.CurrentDecoration == null)
                    return currentAttachment;
            }

            Debug.LogErrorFormat("[WorldAnimal] No available {0} found", attachmentType);
            return null;
        }

        private CritterAttachment FindAvailableCritterAttachment()
        {
            if (critterAttachmentList.Count == 0)
            {
                Debug.LogError("[WorldAnimal] No critter attachments available");
                return null;
            }

            int randomOffset = UnityEngine.Random.Range(0, critterAttachmentList.Count);
            for (int i = 0; i < critterAttachmentList.Count; i++)
            {
                int offsetI = (i + randomOffset) % critterAttachmentList.Count;

                var currentAttachment = critterAttachmentList[offsetI];
                if (currentAttachment.CurrentCritter == null)
                    return currentAttachment;
            }

            Debug.LogError("[WorldAnimal] No available critter attachment found");
            return null;
        }
    }
}