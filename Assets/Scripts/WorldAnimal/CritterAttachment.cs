﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TerraAnimalis
{
    /// <summary>
    /// A location where critters can be attached to.
    /// </summary>
    public class CritterAttachment : MonoBehaviour
    {
        private Critter currentCritter;

        public Critter CurrentCritter { get { return currentCritter; } }

        public void AttachCritter(Critter newCritter)
        {
            if (currentCritter != null)
            {
                Debug.LogError("[CritterAttachment] This attachment is full");
                return;
            }

            var critterTransform = newCritter.transform;
            critterTransform.SetParent(transform, true);
            critterTransform.localPosition = Vector3.zero;
            critterTransform.localRotation = Quaternion.identity;

            currentCritter = newCritter;
            currentCritter.Attachment = this;
        }

        public void RemoveCritter()
        {
            if (currentCritter == null)
            {
                Debug.LogError("[CritterAttachment] No critter to remove");
                return;
            }

            Destroy(currentCritter.gameObject);
            currentCritter = null;
        }

        private void OnDrawGizmos()
        {
            Gizmos.color = Color.red;

            Transform thisTransform = transform;
            Vector3 thisPos = thisTransform.position;
            Gizmos.DrawWireSphere(thisPos, 0.05f);
            Gizmos.DrawLine(thisPos, thisPos + thisTransform.up * 0.2f);
        }
    }
}