﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TerraAnimalis
{
    /// <summary>
    /// Something that can inhabit a world animal.
    /// </summary>
    public class Critter : MonoBehaviour
    {
        private List<WorldAttribute> attributeList;

        public List<WorldAttribute> AttributeList { get { return attributeList; } }

        public CritterAttachment Attachment { get; set; }

        public string CritterName { get; private set; }
        public string QRCode { get; private set; }

        public void Configure(ObjectStatInfo statInfo)
        {
            QRCode = statInfo.QRCode;
            CritterName = statInfo.ObjectName;

            attributeList = new List<WorldAttribute>();
            if(statInfo.Stat1Value != 0)
                attributeList.Add(new WorldAttribute { Attribute = statInfo.Stat1Type, Amount = statInfo.Stat1Value });
            if(statInfo.Stat2Value != 0)
                attributeList.Add(new WorldAttribute { Attribute = statInfo.Stat2Type, Amount = statInfo.Stat2Value });
        }
    }
}