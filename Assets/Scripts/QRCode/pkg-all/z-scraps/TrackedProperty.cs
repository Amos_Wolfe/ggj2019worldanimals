using System;
using N.Package.Events;
using UnityEngine;

namespace GGJ.QRCode
{
  public class TrackedProperty
  {
    private bool internalState;

    private readonly Action onActive;

    private readonly Action onInactive;

    public EventContext Events { get; set; }

    public TrackedProperty(bool initialState, Action onActive, Action onInactive)
    {
      internalState = initialState;
      this.onActive = onActive;
      this.onInactive = onInactive;
    }

    public void Update(bool displayState)
    {
      if (internalState == displayState) return;
      internalState = displayState;
      if (internalState)
      {
        try
        {
          onActive?.Invoke();
        }
        catch (Exception err)
        {
          Debug.LogError(err);
        }
      }
      else
      {
        try
        {
          onInactive?.Invoke();
        }
        catch (Exception err)
        {
          Debug.LogError(err);
        }
      }
    }
  }
}