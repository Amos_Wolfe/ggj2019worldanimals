using System;
using GGJ.QRCode.Z.Package.Capture.Events;
using UnityEngine;

namespace GGJ.QRCode
{
  public class CoolOffEventTrigger
  {
    private readonly Action<CodeScanEvent> _onEvent;

    private CodeScanEvent _pendingItem;

    private float _elapsedTimeSinceLastInput = 0f;

    private CodeScanEvent _lastEvent;

    private readonly float _debounceInterval;
    
    private bool _pendingNull;

    public CoolOffEventTrigger(Action<CodeScanEvent> onEvent, float debounceInterval)
    {
      _onEvent = onEvent;
      _debounceInterval = debounceInterval;
    }

    public void OnEvent(CodeScanEvent item)
    {
      _pendingNull = item.Value == null;
      if (item.Value == null) return;
      _pendingItem = item;
    }

    public void Update()
    {
      // Instantly dispatch new items
      if (_pendingItem != null)
      {
        if (_lastEvent == null || _lastEvent.Value != _pendingItem.Value)
        {
          DispatchEvent();
          return;
        }  
      }
      
      // Otherwise wait
      _elapsedTimeSinceLastInput += Time.deltaTime;
      if (!(_elapsedTimeSinceLastInput > _debounceInterval)) return;
      if (_pendingItem != null)
      {
        DispatchEvent();
      }
    }

    private void DispatchEvent()
    {
      _elapsedTimeSinceLastInput = 0;
      if (!_pendingNull)
      {
        _onEvent(_pendingItem);
        _lastEvent = _pendingItem;
      }
      _pendingItem = null;
      _pendingNull = false;
    }
  }
}