namespace GGJ.QRCode.Z.Package.Capture.Config
{
  [System.Serializable]
  public class CameraSelectionCriteria
  {
    public bool frontFacing;

    public string avoidNamedCamera = "";
  }
}