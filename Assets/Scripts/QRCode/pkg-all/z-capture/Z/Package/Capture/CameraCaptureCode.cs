using GGJ.QRCode.Z.Package.Capture.Events;
using N.Package.Events;
using UnityEngine;
using ZXing;

namespace GGJ.QRCode.Z.Package.Capture
{
  [RequireComponent(typeof(CameraCapture))]
  public class CameraCaptureCode : MonoBehaviour
  {
    public float sampleInterval = 0.5f;

    public string lastDetectedInput = "";

    public string mostRecentValidInput = "";

    private bool _active;

    private float _elapsed;

    private CameraCapture _capture;

    private BarcodeReader _reader;

    public EventHandler EventHandler { get; } = new EventHandler();

    private void Start()
    {
      _capture = GetComponent<CameraCapture>();
      _reader = new BarcodeReader();
    }

    public void Update()
    {
      _elapsed += Time.deltaTime;
      if (!(_elapsed > sampleInterval)) return;
      _elapsed = 0;

      ScanForCodes();
    }

    private void ScanForCodes()
    {
      if (!_active) return;
      if (!_capture.IsActive) return;
      var tex = _capture.GetNativeTexture();
      var value = Decode(tex.GetPixels32(), tex.width, tex.height);
      if (lastDetectedInput != value)
      {
        UpdateSelectedText(value);
      }
    }

    private void UpdateSelectedText(string value)
    {
      lastDetectedInput = value;

      if (value != null)
      {
        mostRecentValidInput = value;
      }

      EventHandler.Trigger(new CodeScanEvent()
      {
        Value = value,
        Scanner = this,
      });
    }
        
    private string Decode(Color32[] colors, int width, int height)
    {
            var newTexture = new Texture2D(width, height);
            newTexture.SetPixels32(colors);

            float widthRatio = (float)width / height;
            TextureScale.Point(newTexture, (int)(64 * widthRatio), 64);

            var result = _reader.Decode(newTexture.GetPixels32(), newTexture.width, newTexture.height);
      return result?.Text;
    }

    public void SetScanning(bool active)
    {
      if (active)
      {
        _capture.Initialize();
      }
      _active = active;
      _capture.SetScanning(active);
      EventHandler.Trigger(new CodeScanStatusEvent()
      {
        Scanner = this,
        Active = _active
      });
    }
  }
}