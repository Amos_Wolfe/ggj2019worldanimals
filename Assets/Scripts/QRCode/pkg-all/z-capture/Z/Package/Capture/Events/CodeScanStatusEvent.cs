namespace GGJ.QRCode.Z.Package.Capture.Events
{
  public class CodeScanStatusEvent
  {
    public bool Active { get; set; }
    public CameraCaptureCode Scanner { get; set; }
  }
}