using System;
using System.Linq;
using GGJ.QRCode.Z.Package.Capture.Config;
using UnityEngine;
using UnityEngine.UI;

namespace GGJ.QRCode.Z.Package.Capture.Infrastructure
{
  public class CameraCaptureTexture
  {
    private readonly WebCamTexture _texture;

    private int _height;

    private int _width;

    private CameraConfig _config;

    private Transform _target;

    public CameraCaptureTexture(CameraConfig config)
    {
      _config = config;
      if (!config.hasActiveCamera)
      {
        Debug.LogError("No available input camera");
        return;
      }

      _texture = MakeWebCamTextureFrom(config);
    }

    public bool Active => _texture != null && _texture.isPlaying;

    public WebCamTexture Native => _texture;

    private WebCamTexture MakeWebCamTextureFrom(CameraConfig config)
    {
      var device = config.GetSelectedDevice();
      var resolution = GetDeviceResolution(config, device);
      var texture = new WebCamTexture(resolution.width, resolution.height) {deviceName = device.name};
      return texture;
    }

    private static Resolution GetDeviceResolution(CameraConfig config, WebCamDevice device)
    {
      return device.availableResolutions?.OrderByDescending(i => i.width).First() ?? new Resolution()
               {width = config.defaultTextureWidth, height = config.defaultTextureHeight};
    }

    public void Stop()
    {
      if (_texture == null) return;
      _texture.Stop();
    }

    public void Start()
    {
      if (_texture == null) return;
      _texture.Play();
    }

    public void Apply(RawImage target)
    {
      if (target == null) return;
      target.texture = _texture;
      ApplyScale(target.transform);
    }

    public void Apply(Renderer target)
    {
      if (target == null) return;
      target.material.mainTexture = _texture;
      ApplyScale(target.transform);
    }

    private void ApplyScale(Transform target)
    {
      _width = _texture.width;
      _height = _texture.height;
      _target = target;
      switch (_config.scaleOutput)
      {
        case CameraScaleOutput.NoScale:
          break;
        case CameraScaleOutput.PreserveHeight:
          var scale = target.localScale;
          var height = scale.y;
          var width = scale.y * _texture.width / _texture.height;
          target.localScale = new Vector3(width, height, scale.z);
          break;
        case CameraScaleOutput.PreserveWidth:
          scale = target.localScale;
          height = scale.x * _texture.height / _texture.width;
          width = scale.x;
          target.localScale = new Vector3(width, height, scale.z);
          break;
        default:
          throw new ArgumentOutOfRangeException();
      }
    }

    public void Update()
    {
      if (_target == null) return;
      if (_width != _texture.width || _height != _texture.height)
      {
        ApplyScale(_target);
      }
    }
  }
}