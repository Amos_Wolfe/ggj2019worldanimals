﻿using GGJ.QRCode.Z.Package.Capture.Config;
using GGJ.QRCode.Z.Package.Capture.Infrastructure;
using UnityEngine;
using UnityEngine.UI;

namespace GGJ.QRCode.Z.Package.Capture
{
  public class CameraCapture : MonoBehaviour
  {
    public bool initializeOnStart = true;

    public CameraConfig config;

    public RawImage target;

    public bool IsActive => _active;
    
    private CameraCaptureTexture _inputTexture;

    private bool _active;

    public void Initialize()
    {
      AutoPopulateTarget();
      config.Initialize();
      _inputTexture = new CameraCaptureTexture(config);
      _inputTexture.Apply(target);
    }

    public void SetScanning(bool active)
    {
      _active = active;
    }

    private void AutoPopulateTarget()
    {
      if (target != null) return;

      target = GetComponent<RawImage>();
      if (target != null) return;

      Debug.Log($"No target specified on {gameObject} and unable to find a default value");
    }

    private void Start()
    {
      if (initializeOnStart)
      {
        Initialize();
      }
    }

    private void Update()
    {
      if (_inputTexture == null) return;
      if (_inputTexture.Active && !_active)
      {
        _inputTexture.Stop();
      }
      else if (!_inputTexture.Active && _active)
      {
        _inputTexture.Start();
      }

      _inputTexture.Update();
    }

    public WebCamTexture GetNativeTexture()
    {
      return _inputTexture.Native;
    }
  }
}