﻿using System.Linq;
using UnityEngine;

namespace GGJ.QRCode.Z.Package.Capture.Config
{
  [System.Serializable]
  public class CameraConfig
  {
    public string[] inputSources;

    public string selectedInput;

    public bool hasActiveCamera;

    public int defaultTextureHeight = 2048;

    public int defaultTextureWidth = 2048;

    public CameraScaleOutput scaleOutput = CameraScaleOutput.PreserveHeight;

    public CameraSelectionCriteria target;

    private WebCamDevice[] _deviceList;

    private WebCamDevice _selectedDevice;

    public void Initialize()
    {
      _deviceList = WebCamTexture.devices;
      inputSources = _deviceList.Select(i => i.name).ToArray();

      hasActiveCamera = SelectBestDevice();
      selectedInput = _selectedDevice.name;
    }

    private bool SelectBestDevice()
    {
      var matchScore = -1f;
      var match = default(WebCamDevice);
      foreach (var device in _deviceList)
      {
        var score = MatchScore(device);
        if (!(score > matchScore)) continue;
        match = device;
        matchScore = score;
      }

      _selectedDevice = match;
      return matchScore >= 0f;
    }

    private float MatchScore(WebCamDevice device)
    {
      var value = 0f;
      if (device.isFrontFacing && target.frontFacing)
      {
        value += 1;
      }
      
      if (string.IsNullOrWhiteSpace(target.avoidNamedCamera) && !device.name.ToLower().Contains(target.avoidNamedCamera))
      {
        value += 1;
      }

      return value;
    }

    public WebCamDevice GetSelectedDevice()
    {
      return _selectedDevice;
    }
  }
}