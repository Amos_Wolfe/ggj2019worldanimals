namespace GGJ.QRCode.Z.Package.Capture.Events
{
  public class CodeScanEvent
  {
    public string Value { get; set; }
    public CameraCaptureCode Scanner { get; set; }
  }
}