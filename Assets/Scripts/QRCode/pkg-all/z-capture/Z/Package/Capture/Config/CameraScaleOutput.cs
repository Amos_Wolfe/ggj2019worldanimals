namespace GGJ.QRCode.Z.Package.Capture.Config
{
  public enum CameraScaleOutput
  {
    NoScale,
    PreserveHeight,
    PreserveWidth
  }
}