﻿using GGJ.QRCode.Z.Package.Capture.Events;
using UnityEngine;
using UnityEngine.UI;

namespace GGJ.QRCode.Z.Package.Capture
{
  public class CameraCaptureDebug : MonoBehaviour
  {
    public Text debugOutput;

    public void OnScanEvent(CodeScanEvent codeScanEvent)
    {
      debugOutput.text = codeScanEvent.Value;
    }
  }
}