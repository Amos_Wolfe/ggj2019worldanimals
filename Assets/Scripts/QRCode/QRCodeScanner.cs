using System;
using GGJ.QRCode.Z.Package.Capture;
using GGJ.QRCode.Z.Package.Capture.Events;
using N.Package.Events;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace GGJ.QRCode
{
  [System.Serializable]
  public class QRCodeScannerEvent : UnityEvent<CodeScanEvent>
  {
  }

  public class QRCodeScanner : MonoBehaviour
  {
    public CameraCaptureCode capture;

    public CameraCaptureDebug debug;

    public bool showDebuggingTool = true;

    public bool activeScan = true;

    public float scanInterval = 0.5f;

    [Tooltip("Dont trigger the same card with this interval of triggering it")]
    public float debounceEventInterval = 3f;

    public QRCodeScannerEvent onEvent;

    private bool failed;

    private TrackedProperty debugging;

    private TrackedProperty scanning;

    private CoolOffEventTrigger dispatcher;
    
    public void Start()
    {
      try
      {
        RequireMandatoryComponents();
      }
      catch (Exception)
      {
        failed = true;
        throw;
      }

      debugging = new TrackedProperty(false, EnableDebugging, DisableDebugging);
      scanning = new TrackedProperty(false, EnableScanning, DisableScanning);
      dispatcher = new CoolOffEventTrigger(DispatchEvent, debounceEventInterval);
    }

    public void Update()
    {
      if (failed) return;
      debugging.Update(showDebuggingTool);
      scanning.Update(activeScan);
      dispatcher.Update();
    }

    public void EnableScanning()
    {
      scanning.Events = new EventContext();
      capture.EventHandler.AddEventHandler<CodeScanEvent>((e) => dispatcher.OnEvent(e), scanning.Events);
      capture.sampleInterval = scanInterval;
      capture.SetScanning(true);
    }

    public void DisableScanning()
    {
      scanning.Events.Dispose();
      scanning.Events = null;
      capture.SetScanning(false);
    }

    public void EnableDebugging()
    {
      debugging.Events = new EventContext();
      debug.gameObject.SetActive(true);
      capture.EventHandler.AddEventHandler<CodeScanEvent>((e) => { debug.OnScanEvent(e); }, debugging.Events);
    }

    public void DisableDebugging()
    {
      debug.gameObject.SetActive(false);
      debugging.Events.Dispose();
      debugging.Events = null;
    }

    private void RequireMandatoryComponents()
    {
      var eventSystem = FindObjectOfType<EventSystem>();
      if (eventSystem == null) throw new Exception($"Hey! you can't use {gameObject} unless you have an EventSystem on the scene!");
    }

    private void DispatchEvent(CodeScanEvent codeScanEvent)
    {
      Debug.Log($"Got a value: {codeScanEvent.Value}");
      onEvent.Invoke(codeScanEvent);
    }
  }
}