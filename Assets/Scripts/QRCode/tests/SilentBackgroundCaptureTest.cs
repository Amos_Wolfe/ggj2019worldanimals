﻿using GGJ.QRCode.Z.Package.Capture.Events;
using UnityEngine;

namespace GGJ.QRCode.tests
{
  public class SilentBackgroundCaptureTest : MonoBehaviour
  {
    public void OnCodeScanned(CodeScanEvent input)
    {
      Debug.Log(input.Value);
    }
  }
}