using GGJ.QRCode;
using GGJ.QRCode.Z.Package.Capture.Events;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace DefaultNamespace
{
    public class RuntimeDebugging : MonoBehaviour
    {
        public CanvasGroup debugMenuObject;

        public QRCodeScanner scanner;

        public Dropdown animalPicker;
        
        public Dropdown inhabitantPicker;
        
        public Dropdown decorPicker;

        private bool debugMenuEnabled = false;

        private bool debugCamera = false;

        private bool onCamera = false;

        private TerraAnimalis.QRMapper manager;

        public void Start()
        {
            scanner = FindObjectOfType<QRCodeScanner>();
            manager = FindObjectOfType<TerraAnimalis.QRMapper>();

            SetVisible(debugMenuObject, false);
            scanner.showDebuggingTool = false;
        }

        public void Reset()
        {
            var scene = SceneManager.GetActiveScene();
            SceneManager.LoadScene(scene.name);
        }

        public void ToggleDebugMenu()
        {
            debugMenuEnabled = !debugMenuEnabled;
            SetVisible(debugMenuObject, debugMenuEnabled);
        }

        public void ToggleCameraDebug()
        {
            debugCamera = !debugCamera;
            scanner.showDebuggingTool = debugCamera;
        }
        
        public void ToggleCameraState()
        {
            onCamera = !onCamera;
            scanner.activeScan = onCamera;
        }

        public void TriggerAnimalEvent()
        {
            var value = animalPicker.options[animalPicker.value];
            manager.OnCodeScanEvent(new CodeScanEvent() {Value = value.text});
            ToggleDebugMenu();
        }
        
        public void TriggerInhabitEvent()
        {
            var value = inhabitantPicker.options[inhabitantPicker.value];
            manager.OnCodeScanEvent(new CodeScanEvent() {Value = value.text});
            ToggleDebugMenu();
        }
        
        public void TriggerDecorEvent()
        {
            var value = decorPicker.options[decorPicker.value];
            manager.OnCodeScanEvent(new CodeScanEvent() {Value = value.text});
            ToggleDebugMenu();
        }

        private void SetVisible(CanvasGroup canvasGroup, bool isVisible)
        {
            canvasGroup.interactable = isVisible;
            canvasGroup.blocksRaycasts = isVisible;
            canvasGroup.alpha = isVisible ? 1 : 0;
            if (isVisible && !canvasGroup.gameObject.activeInHierarchy)
            {
                canvasGroup.gameObject.SetActive(true);
            }
        }
    }
}