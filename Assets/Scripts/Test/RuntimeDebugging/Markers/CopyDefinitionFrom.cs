using System;
using UnityEngine;
using UnityEngine.UI;

namespace Test.RuntimeDebugging.Markers
{
    [ExecuteInEditMode]
    [RequireComponent(typeof(Dropdown))]
    public class CopyDefinitionFrom : MonoBehaviour
    {
        public TerraAnimalis.QRMapper mapper;

        public CopyFromSource mode;

        public bool execute;

        public void Update()
        {
            if (!execute) return;
            if (mapper == null) return;

            execute = false;
            var dropdown = GetComponent<Dropdown>();
            switch (mode)
            {
                case CopyFromSource.CopyWorldAnimalIds:
                    dropdown.options.Clear();
                    foreach (var item in mapper.worldAnimalList)
                    {
                        dropdown.options.Add(new Dropdown.OptionData(item.QRCode));
                    }
                    break;
                case CopyFromSource.CopyCritterIds:
                    dropdown.options.Clear();
                    foreach (var item in mapper.critterList)
                    {
                        dropdown.options.Add(new Dropdown.OptionData(item.QRCode));
                    }
                    break;
                case CopyFromSource.CopyDecoIds:
                    dropdown.options.Clear();
                    foreach (var item in mapper.decorationList)
                    {
                        dropdown.options.Add(new Dropdown.OptionData(item.QRCode));
                    }
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }

    public enum CopyFromSource
    {
        CopyCritterIds,
        CopyDecoIds,
        CopyWorldAnimalIds
    }
}