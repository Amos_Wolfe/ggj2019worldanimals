using UnityEngine;

namespace DefaultNamespace
{
    public class CloseCameraPopupOnClick : MonoBehaviour
    {
        private RuntimeDebugging _debug;

        public void Start()
        {
            _debug = FindObjectOfType<RuntimeDebugging>();
        }

        public void OnCloseClick()
        {
            _debug.ToggleCameraDebug();
        }
    }
}