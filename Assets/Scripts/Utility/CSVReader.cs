﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TerraAnimalis
{
    public class ObjectStatInfo
    {
        public string QRCode;

        public string Stat1Type;
        public int Stat1Value;

        public string Stat2Type;
        public int Stat2Value;

        public string ObjectName;
    }

    /// <summary>
    /// Reads the stats for each critter and decoration from a csv file.
    /// </summary>
    public class CSVReader : MonoBehaviour
    {
        // TODO send errors if the number of stat lines do not match the number of stats in the QR mapper


        [SerializeField]
        private TextAsset worldAnimals;
        [SerializeField]
        private TextAsset decorations;
        [SerializeField]
        private TextAsset critters;

        private void Awake()
        {
            var newStats = ReadCSV(critters);

            Debug.Log("Read stats");
        }

        public List<ObjectStatInfo> GetWorldAnimals()
        {
            return ReadCSV(worldAnimals);
        }

        public List<ObjectStatInfo> GetCritters()
        {
            return ReadCSV(critters);
        }

        public List<ObjectStatInfo> GetDecorations()
        {
            return ReadCSV(decorations);
        }

        private List<ObjectStatInfo> ReadCSV(TextAsset targetTextAsset)
        {
            var lineArray = targetTextAsset.text.Split(new string[] { System.Environment.NewLine },
        System.StringSplitOptions.RemoveEmptyEntries);

            var csvList = new List<ObjectStatInfo>();
            for (int i = 1; i < lineArray.Length; i++)
            {
                csvList.Add(ReadStatLine(lineArray[i]));
            }

            return csvList;
        }

        public ObjectStatInfo ReadStatLine(string statLine)
        {
            var statList = statLine.Split(',');

            return new ObjectStatInfo
            {
                QRCode = statList[0],
                Stat1Value = ParseInt(statList[1].Trim()),
                Stat1Type = statList[2].Trim(),
                Stat2Value = ParseInt(statList[3].Trim()),
                Stat2Type = statList[4].Trim(),
                ObjectName = statList[5].Trim(),
            };
        }

        public int ParseInt(string intString)
        {
            int newValue;

            if (!int.TryParse(intString, out newValue))
            {
                Debug.LogErrorFormat("Could not read int value '{0}'", intString);
                return 0;
            }
            else
            {
                return newValue;
            }
        }
    }
}