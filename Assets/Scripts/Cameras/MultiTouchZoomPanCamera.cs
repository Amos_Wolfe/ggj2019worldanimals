﻿using UnityEngine;

namespace Cameras
{
    public class MultiTouchZoomPanCamera : MonoBehaviour
    {
        public float panSpeed = 20f;
        public float zoomSpeedTouch = 0.1f;
        public float zoomSpeedMouse = 0.5f;

        public float[] maxMoveX = new float[] {3f, 3f};
        public float[] maxMoveY = new float[] {3f, 3f};
        public float[] zoomBounds = new float[] {1f, 4f};

        private Camera localCamera;

        private Vector3 lastPanPosition;
        private int panFingerId; // Touch mode only

        private bool wasZoomingLastFrame; // Touch mode only
        private Vector2[] lastZoomPositions; // Touch mode only
        private Vector3 origin;

        void Awake()
        {
            origin = transform.position;
            localCamera = GetComponent<Camera>();
        }

        void Update()
        {
            if (Input.touchSupported && Application.platform != RuntimePlatform.WebGLPlayer)
            {
                HandleTouch();
            }
            else
            {
                HandleMouse();
            }
        }

        void HandleTouch()
        {
            switch (Input.touchCount)
            {
                case 1: // Panning
                    wasZoomingLastFrame = false;

                    // If the touch began, capture its position and its finger ID.
                    // Otherwise, if the finger ID of the touch doesn't match, skip it.
                    Touch touch = Input.GetTouch(0);
                    if (touch.phase == TouchPhase.Began)
                    {
                        lastPanPosition = touch.position;
                        panFingerId = touch.fingerId;
                    }
                    else if (touch.fingerId == panFingerId && touch.phase == TouchPhase.Moved)
                    {
                        PanCamera(touch.position);
                    }

                    break;

                case 2: // Zooming
                    Vector2[] newPositions = new Vector2[] {Input.GetTouch(0).position, Input.GetTouch(1).position};
                    if (!wasZoomingLastFrame)
                    {
                        lastZoomPositions = newPositions;
                        wasZoomingLastFrame = true;
                    }
                    else
                    {
                        // Zoom based on the distance between the new positions compared to the 
                        // distance between the previous positions.
                        float newDistance = Vector2.Distance(newPositions[0], newPositions[1]);
                        float oldDistance = Vector2.Distance(lastZoomPositions[0], lastZoomPositions[1]);
                        float offset = newDistance - oldDistance;

                        ZoomCamera(offset, zoomSpeedTouch);

                        lastZoomPositions = newPositions;
                    }

                    break;

                default:
                    wasZoomingLastFrame = false;
                    break;
            }
        }

        void HandleMouse()
        {
            // On mouse down, capture it's position.
            // Otherwise, if the mouse is still down, pan the camera.
            if (Input.GetMouseButtonDown(0))
            {
                lastPanPosition = Input.mousePosition;
            }
            else if (Input.GetMouseButton(0))
            {
                PanCamera(Input.mousePosition);
            }

            // Check for scrolling to zoom the camera
            float scroll = Input.GetAxis("Mouse ScrollWheel");
            ZoomCamera(scroll, zoomSpeedMouse);
        }

        void PanCamera(Vector3 newPanPosition)
        {
            // Determine how much to move the camera
            Vector3 offset = localCamera.ScreenToViewportPoint(lastPanPosition - newPanPosition);
            Vector3 move = new Vector3(offset.x * panSpeed, offset.y * panSpeed, 0);

            // Perform the movement
            var cameraState = gameObject.transform;
            cameraState.Translate(move, Space.World);

            // Ensure the camera remains within bounds.
            Vector3 cameraNewPos = cameraState.position;
            cameraNewPos.x = Mathf.Clamp(cameraNewPos.x, origin.x - maxMoveX[0], origin.x + maxMoveX[1]);
            cameraNewPos.y = Mathf.Clamp(cameraNewPos.y, origin.y - maxMoveY[0], origin.y + maxMoveY[1]);
            transform.position = cameraNewPos;

            // Cache the position
            lastPanPosition = newPanPosition;
        }

        void ZoomCamera(float offset, float speed)
        {
            if (offset == 0)
            {
                return;
            }

            localCamera.orthographicSize = Mathf.Clamp(localCamera.orthographicSize - (offset * speed), zoomBounds[0], zoomBounds[1]);
        }
    }
}