using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace SceneFragments
{
    public class SceneFragmentsManager : MonoBehaviour
    {
        public List<GameObject> scoreBoard;
        public List<GameObject> intro;
        public List<GameObject> credits;

        public void Awake()
        {
            scoreBoard = FindMarkerOfType(SceneFragmentId.ScoreBoard);
            intro = FindMarkerOfType(SceneFragmentId.Intro);
            credits = FindMarkerOfType(SceneFragmentId.Credits);

            SetActive(scoreBoard, false);
            SetActive(credits, false);
            SetActive(intro, true);
        }

        public void SetActive(IEnumerable<GameObject> targets, bool isActive)
        {
            foreach (var target in targets)
            {
                target.SetActive(isActive);
            }
        }

        private List<GameObject> FindMarkerOfType(SceneFragmentId sceneFragmentId)
        {
            return Resources.FindObjectsOfTypeAll<SceneFragmentMarker>()
                .Where(instance => instance.id == sceneFragmentId)
                .Select(i => i.gameObject)
                .ToList();
        }

        public void Toggle(List<GameObject> gameObjects)
        {
            foreach (var target in gameObjects)
            {
                var state = !target.activeInHierarchy;
                target.SetActive(state);
            }
        }
    }
}