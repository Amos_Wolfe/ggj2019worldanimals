namespace SceneFragments
{
    public enum SceneFragmentId
    {
        ScoreBoard,
        Intro,
        Credits
    }
}