using UnityEngine;

namespace SceneFragments
{
    public class SceneFragmentsInputs : MonoBehaviour
    {
        private SceneFragmentsService _service;

        public void Start()
        {
            _service = new SceneFragmentsService();
        }
        
        public void ToggleHelpAbout()
        {
            _service.ToggleHelp();
        }
    }
}