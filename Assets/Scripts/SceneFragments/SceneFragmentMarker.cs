using UnityEngine;

namespace SceneFragments
{
    public class SceneFragmentMarker : MonoBehaviour
    {
        public SceneFragmentId id;
    }
}