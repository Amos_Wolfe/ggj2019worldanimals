using System;
using UnityEngine;
using Object = UnityEngine.Object;

namespace SceneFragments
{
    public class SceneFragmentsService
    {
        private SceneFragmentsManager _manager;

        public SceneFragmentsService()
        {
            _manager = Object.FindObjectOfType<SceneFragmentsManager>();
            if (_manager == null)
            {
                throw new Exception("You need an SceneFragmentsManager on the scene to use the SceneFragmentsService.");
            }
        }

        public void HideIntro()
        {
            _manager.SetActive(_manager.intro, false);
            _manager.SetActive(_manager.scoreBoard, true);
        }

        public void ToggleHelp()
        {
            _manager.Toggle(_manager.credits);
        }
    }
}