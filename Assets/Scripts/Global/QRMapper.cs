﻿using GGJ.QRCode.Z.Package.Capture.Events;
using System.Collections;
using System.Collections.Generic;
using Audio;
using UnityEngine;

namespace TerraAnimalis
{
    /// <summary>
    /// Makes QR code scan events to the GameManager
    /// </summary>
    public class QRMapper : MonoBehaviour
    {
        [HideInInspector]
        public List<ObjectStatInfo> worldAnimalList;
        [HideInInspector]
        public List<ObjectStatInfo> decorationList;
        [HideInInspector]
        public List<ObjectStatInfo> critterList;

        private WorldAnimalFactory worldAnimalFactory;
        private WorldDecorationFactory decorationFactory;
        private CritterFactory critterFactory;
        

        private GameManager gameManager;
        private AudioService audioService;
        private CSVReader csvReader;

        private void Awake()
        {
            worldAnimalFactory = GameObject.FindObjectOfType<WorldAnimalFactory>();
            decorationFactory = GameObject.FindObjectOfType<WorldDecorationFactory>();
            critterFactory = GameObject.FindObjectOfType<CritterFactory>();
            

            gameManager = GetComponent<GameManager>();
            csvReader = GetComponent<CSVReader>();
            audioService = new AudioService();

            ReadWorldAnimalList();
            ReadDecorationList();
            ReadCritterList();
        }

        private void ReadWorldAnimalList()
        {
            worldAnimalList = csvReader.GetWorldAnimals();

            for (int i = 0; i < worldAnimalList.Count; i++)
            {
                if (!worldAnimalFactory.DoesWorldAnimalExist(worldAnimalList[i].ObjectName))
                {
                    Debug.LogErrorFormat("World Animal with name {0} does not exist", worldAnimalList[i].ObjectName);
                }
            }
        }

        private void ReadCritterList()
        {
            critterList = csvReader.GetCritters();

            for (int i = 0; i < critterList.Count; i++)
            {
                if (!critterFactory.DoesCritterExist(critterList[i].ObjectName))
                {
                    Debug.LogErrorFormat("Critter with name {0} does not exist", critterList[i].ObjectName);
                }
            }
        }

        private void ReadDecorationList()
        {
            decorationList = csvReader.GetDecorations();

            for (int i = 0; i < decorationList.Count; i++)
            {
                if (!decorationFactory.DoesDecorationExist(decorationList[i].ObjectName))
                {
                    Debug.LogErrorFormat("Decoration with name {0} does not exist", decorationList[i].ObjectName);
                }
            }
        }

        public void OnCodeScanEvent(CodeScanEvent scanEvent)
        {
            foreach (var worldAnimal in worldAnimalList)
            {
                if (worldAnimal.QRCode == scanEvent.Value)
                {
                    gameManager.SetAnimal(worldAnimal);
                    TriggerPostScanEvents();
                    return;
                }
            }

            foreach (var decoration in decorationList)
            {
                if (decoration.QRCode == scanEvent.Value)
                {
                    gameManager.AttachOrRemoveDecoration(decoration);
                    TriggerPostScanEvents();
                    return;
                }
            }

            foreach (var critter in critterList)
            {
                if (critter.QRCode == scanEvent.Value)
                {
                    gameManager.AttachOrRemoveCritter(critter);
                    TriggerPostScanEvents();
                    return;
                }
            }
        }

        /// <summary>
        /// Trigger any arbitrary events for after a code has been scanned
        /// </summary>
        private void TriggerPostScanEvents()
        {
            audioService.PlayOneShot(AudioSourceId.GlobalAudioEffects, AudioId.ScannedCodeEvent);
        }
    }
}