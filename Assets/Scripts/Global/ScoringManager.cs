﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace TerraAnimalis
{
    public class ScoringManager : MonoBehaviour
    {
        [SerializeField]
        private Text worldAnimalNameLabel;
        [SerializeField]
        private Text scoreLabel;

        [SerializeField]
        private bool debugGUI;
        [SerializeField]
        private Text debugLabel;

        private List<WorldAttribute> attributeList;
        
        private StringBuilder statsDebugString;
        private StringBuilder scoreDebugString;

        private List<int> critterScores;

        private void Awake()
        {
            scoreLabel.text = string.Empty;

            attributeList = new List<WorldAttribute>();
            
            statsDebugString = new StringBuilder();
            scoreDebugString = new StringBuilder();

            critterScores = new List<int>();
        }

        public void UpdateName(WorldAnimal worldAnimal)
        {
            var decorationList = worldAnimal.Decorations;
            string nameString = "";

            for(int i = 0; i < decorationList.Count; i++)
            {
                var currentDecoration = decorationList[i];
                string newAdjective = currentDecoration.Adjective;
                if (string.IsNullOrEmpty(newAdjective)) continue;
                
                if (!nameString.Contains(newAdjective))
                {
                    if (nameString.Length > 0)
                        nameString += " ";

                    nameString += newAdjective;
                }
                else
                    nameString = nameString.Replace(newAdjective, "Very " + newAdjective);
            }

            if (nameString.Length > 0)
                nameString += " ";

            nameString += worldAnimal.AnimalName;

            worldAnimalNameLabel.text = nameString.ToString();
        }

        public void UpdateScore(WorldAnimal worldAnimal)
        {
            if (debugGUI)
            {
                statsDebugString.Clear();
                scoreDebugString.Clear();
                critterScores.Clear();
            }

            CollectAttributes(worldAnimal);

            if (debugGUI)
            {
                statsDebugString.AppendLine("Critters:");

                if (worldAnimal.Critters.Count == 0)
                    statsDebugString.AppendLine("None");

                scoreDebugString.AppendLine("Critters");
                scoreDebugString.AppendLine();
            }

            int totalScore = 0;
            var critterList = worldAnimal.Critters;
            for (int i = 0; i < critterList.Count; i++)
            {
                var critter = critterList[i];
                totalScore += CalculateCritterScore(critter);
            }

            if (debugGUI)
            {
                scoreDebugString.AppendLine();
                scoreDebugString.Append("Total score for critters is: ");
                for (int i = 0; i < critterScores.Count; i++)
                {
                    scoreDebugString.Append(critterScores[i]);
                    if (i != critterScores.Count - 1)
                        scoreDebugString.Append(" + ");
                }

                scoreDebugString.AppendFormat(" = {0}", totalScore);
                scoreDebugString.AppendLine();
                scoreDebugString.AppendLine();

                statsDebugString.AppendLine();
                statsDebugString.AppendLine("Modifiers:");
                if (worldAnimal.Modifiers.Count == 0)
                    statsDebugString.AppendLine("None");

                if (worldAnimal.Modifiers.Count != 0)
                {
                    scoreDebugString.AppendLine("Modifiers");
                    scoreDebugString.AppendLine();
                }
            }

            int scoreAddedByModifiers = 0;

            var modifierList = worldAnimal.Modifiers;
            for (int i = 0; i < modifierList.Count; i++)
            {
                int totalScoreBefore = totalScore;
                totalScore = modifierList[i].ModifyScore(worldAnimal, totalScore, debugGUI, ref statsDebugString, ref scoreDebugString);
                scoreAddedByModifiers += totalScore - totalScoreBefore;

                if (debugGUI)
                {
                    statsDebugString.AppendLine();
                    statsDebugString.AppendLine();
                    scoreDebugString.AppendLine();
                }
            }

            if (debugGUI && modifierList.Count != 0)
            {
                scoreDebugString.AppendFormat("Total score added by all modifiers is {0}", scoreAddedByModifiers);
                scoreDebugString.AppendLine();
                scoreDebugString.AppendLine();
                scoreDebugString.AppendFormat("Final score: {0} + {1} = {2}", totalScore - scoreAddedByModifiers, scoreAddedByModifiers, totalScore);
            }

            scoreLabel.text = totalScore.ToString();

            if (debugGUI)
                debugLabel.text = string.Format("<color=#aaaaaa>Stats</color>\n\n{0}\n\n<color=#aaaaaa>Score Calculation</color>\n\n{1}", statsDebugString.ToString(), scoreDebugString.ToString());
        }

        private void CollectAttributes(WorldAnimal worldAnimal)
        {
            attributeList.Clear();

            if (debugGUI)
            {
                statsDebugString.AppendLine("Base Attributes (From World Animal):");

                if (worldAnimal.BaseAttributes.Count == 0)
                    statsDebugString.AppendLine("None");

                for (int i = 0; i < worldAnimal.BaseAttributes.Count; i++)
                {
                    var attribute = worldAnimal.BaseAttributes[i];
                    statsDebugString.AppendFormat("+{0} {1}", attribute.Amount, attribute.Attribute);
                    statsDebugString.AppendLine();
                }

                statsDebugString.AppendLine();
            }

            attributeList.AddRange(worldAnimal.BaseAttributes);

            if (debugGUI)
            {
                statsDebugString.AppendLine("Decorations:");
                if (worldAnimal.Decorations.Count == 0)
                    statsDebugString.AppendLine("None");
            }

            var decorations = worldAnimal.Decorations;
            for (int i = 0; i < decorations.Count; i++)
            {
                var decoration = decorations[i];
                attributeList.AddRange(decoration.AttributeList);

                if (debugGUI)
                {
                    statsDebugString.Append(decoration.DecorationName);

                    foreach (var decorAttribute in decoration.AttributeList)
                    {
                        if (decorAttribute.Amount > 0)
                            statsDebugString.AppendFormat(" +{0} {1}", decorAttribute.Amount, decorAttribute.Attribute);
                        else
                            statsDebugString.AppendFormat(" {0} {1}", decorAttribute.Amount, decorAttribute.Attribute);
                    }
                }

                if (debugGUI)
                    statsDebugString.AppendLine();
            }

            statsDebugString.AppendLine();
        }

        private int CalculateCritterScore(Critter critter)
        {
            var critterAttributes = critter.AttributeList;

            if (debugGUI)
                statsDebugString.Append(critter.CritterName);

            int totalScore = 0;
            for (int i = 0; i < critterAttributes.Count; i++)
            {
                var currentAttribute = critterAttributes[i];
                int amountOnWorldAnimal = GetAttributeTypeAmount(currentAttribute.Attribute);

                int additionalScore = currentAttribute.Amount * amountOnWorldAnimal;

                if (debugGUI)
                {
                    if (amountOnWorldAnimal != 0)
                    {
                        scoreDebugString.AppendFormat("{4} | {0} | {1} x {2} = {3}", currentAttribute.Attribute, currentAttribute.Amount, amountOnWorldAnimal, additionalScore, critter.CritterName);
                        scoreDebugString.AppendLine();

                        critterScores.Add(additionalScore);
                    }
                }

                totalScore += additionalScore;

                if (debugGUI)
                {
                    if (currentAttribute.Amount > 0)
                        statsDebugString.AppendFormat(" +{0} {1}", currentAttribute.Amount, currentAttribute.Attribute);
                    else
                        statsDebugString.AppendFormat(" {0} {1}", currentAttribute.Amount, currentAttribute.Attribute);
                }
            }

            if (debugGUI)
                statsDebugString.AppendLine();

            return totalScore;
        }

        private int GetAttributeTypeAmount(string attributeType)
        {
            int amount = 0;
            for (int i = 0; i < attributeList.Count; i++)
            {
                var attribute = attributeList[i];
                if (attribute.Attribute == attributeType)
                    amount += attribute.Amount;
            }

            return amount;
        }
    }
}