﻿public enum AttachmentType
{
    OnTop,
    InFront,
    Satellite,
}