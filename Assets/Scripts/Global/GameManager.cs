﻿using System.Collections;
using System.Collections.Generic;
using GGJ.QRCode.Z.Package.Capture.Events;
using SceneFragments;
using UnityEngine;

namespace TerraAnimalis
{
    public class GameManager : MonoBehaviour
    {
        private WorldAnimalFactory worldAnimalFactory;

        private WorldAnimal worldAnimal;

        private SceneFragmentsService sceneService;

        private void Awake()
        {
            worldAnimalFactory = GameObject.FindObjectOfType<WorldAnimalFactory>();
            sceneService = new SceneFragmentsService();
            InitializeGlobalSettings();
        }

        /// <summary>
        /// Changes the animal used for the player. Note this will get rid of all attachments on the animal.
        /// </summary>
        public void SetAnimal(ObjectStatInfo animalInfo)
        {
            if (worldAnimal != null)
            {
                if (worldAnimal.name.Trim() == animalInfo.ObjectName.Trim()) return;
                Destroy(worldAnimal.gameObject);
            }

            sceneService.HideIntro(); // Do this first so the animal can find it when it spawns
            worldAnimal = worldAnimalFactory.CreateAnimal(animalInfo.ObjectName);
            worldAnimal.Configure(animalInfo);
        }

        /// <summary>
        /// Add a decoration with an ID. if an decoration with that ID already exists on the world animal, then it is removed.
        /// </summary>
        public void AttachOrRemoveDecoration(ObjectStatInfo decorationInfo)
        {
            if (worldAnimal == null)
            {
                Debug.LogError("[GameManager] World animal not set yet");
                return;
            }

            worldAnimal.AttachOrRemoveDecoration(decorationInfo);
        }

        public void AttachOrRemoveCritter(ObjectStatInfo critterInfo)
        {
            if (worldAnimal == null)
            {
                Debug.LogError("[GameManager] World animal not set yet");
                return;
            }

            worldAnimal.AttachOrRemoveCritter(critterInfo);
        }

        private void InitializeGlobalSettings()
        {
            Screen.orientation = ScreenOrientation.LandscapeLeft;
        }
    }
}