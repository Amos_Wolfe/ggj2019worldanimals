using System.Collections.Generic;
using UnityEngine;

namespace Audio
{
    public class AudioManagerConfig : MonoBehaviour
    {
        public List<AudioSourceBinding> sources;
        public List<AudioBinding> clips;
    }
}