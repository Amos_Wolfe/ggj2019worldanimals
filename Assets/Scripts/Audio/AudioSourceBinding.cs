using UnityEngine;

namespace Audio
{
    [System.Serializable]
    public class AudioSourceBinding
    {
        public AudioSourceId id;
        public AudioSource source;
    }
}