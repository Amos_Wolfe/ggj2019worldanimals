using UnityEngine;

namespace Audio
{
    [System.Serializable]
    public class AudioBinding
    {
        public AudioId id;
        public AudioClip clip;
    }
}