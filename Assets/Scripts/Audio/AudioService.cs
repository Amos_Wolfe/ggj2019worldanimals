using System;
using System.Linq;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Audio
{
    public class AudioService
    {
        private readonly AudioManagerConfig _config;

        public AudioService()
        {
            _config = Object.FindObjectOfType<AudioManagerConfig>();
            if (_config == null)
            {
                throw new Exception("You need an AudioManagerConfig on the scene to use the audio manager service.");
            }
        }

        public void PlayOneShot(AudioSourceId source, AudioId clip)
        {
            var audioSource = _config.sources.FirstOrDefault(i => i.id == source);
            var audioClip = _config.clips.FirstOrDefault(i => i.id == clip);
            if (audioSource == null || audioClip == null)
            {
                Debug.LogError($"AudioService: No match to play {clip} from {source}");
                return;
            }

            audioSource.source.PlayOneShot(audioClip.clip);
        }
    }
}