﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TerraAnimalis
{
    public class CritterFactory : MonoBehaviour
    {
        [SerializeField]
        private List<Critter> objects;

        public Critter CreateCritter(string critterName)
        {
            var critterTemplate = GetCritterTemplate(critterName);
            if (critterTemplate == null)
                return null;

            var newCritter = Instantiate(critterTemplate);
            return newCritter;
        }

        public Critter GetCritterTemplate(string critterName)
        {
            for (int i = 0; i < objects.Count; i++)
            {
                var currentObject = objects[i];
                if (currentObject.name == critterName)
                    return currentObject;
            }

            Debug.LogErrorFormat("[CritterFactory] Cannot find critter with name {0}", critterName);
            return null;
        }

        public bool DoesCritterExist(string critterName)
        {
            for (int i = 0; i < objects.Count; i++)
            {
                if (objects[i].name == critterName)
                    return true;
            }
            return false;
        }
    }
}