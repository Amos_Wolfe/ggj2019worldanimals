﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TerraAnimalis
{
    public class WorldAnimalFactory : MonoBehaviour
    {
        [SerializeField]
        private List<WorldAnimal> objects;

        public WorldAnimal CreateAnimal(string animalname)
        {
            var animalTemplate = GetAnimalInfo(animalname);
            if (animalTemplate == null)
                return null;

            var newAnimal = Instantiate(animalTemplate);
            return newAnimal;
        }

        public WorldAnimal GetAnimalInfo(string animalName)
        {
            for (int i = 0; i < objects.Count; i++)
            {
                var currentObject = objects[i];
                if (currentObject.name == animalName)
                    return currentObject;
            }

            Debug.LogErrorFormat("[WorldAnimalFactory] Cannot find animal with name {0}", animalName);
            return null;
        }

        public bool DoesWorldAnimalExist(string animalName)
        {
            for (int i = 0; i < objects.Count; i++)
            {
                if (objects[i].name == animalName)
                    return true;
            }
            return false;
        }
    }
}