﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TerraAnimalis
{
    public class WorldDecorationFactory : MonoBehaviour
    {
        [SerializeField]
        private List<WorldDecoration> objects;

        public WorldDecoration CreateDecoration(string decorationName)
        {
            var decorationTemplate = GetDecorationInfo(decorationName);
            if (decorationTemplate == null)
                return null;

            var newDecoration = Instantiate(decorationTemplate);
            return newDecoration;
        }

        public WorldDecoration GetDecorationInfo(string decorationName)
        {
            for (int i = 0; i < objects.Count; i++)
            {
                var currentObject = objects[i];
                if (currentObject.name == decorationName)
                    return currentObject;
            }

            Debug.LogErrorFormat("[WorldDecorationFactory] Cannot find decoration type of {0}", decorationName);
            return null;
        }

        public bool DoesDecorationExist(string decorationName)
        {
            for (int i = 0; i < objects.Count; i++)
            {
                if (objects[i].name == decorationName)
                    return true;
            }
            return false;
        }
    }
}